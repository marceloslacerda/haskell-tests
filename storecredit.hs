import Data.Char(digitToInt)
import Text.Printf

main = interact (unlines . solve . lines)

solve (_:xs) = solveaux xs 1

solveaux :: [String] -> Int -> [String]
solveaux [] _ = []
solveaux (credit:_:items: xs) idx =
    (strSelectedItems (read credit :: Int) $ parseItems items) : solveaux xs (idx + 1)
    where strSelectedItems c is = toResult (selectedItems c is) idx

toResult :: (Int, Int) -> Int -> String
toResult (x,y) i = printf "Case #%d: %d %d" i x y

selectedItems :: Int -> [Int] -> (Int, Int)
selectedItems cred items = 
    select cred $ comb 2 [1..(length items)]
    where select c [] = (1,1)
          select c ([i1, i2]:ps) = if ((items !! (i1-1)) + items !! (i2-1)) == c 
                                   then (i1, i2)
                                   else select c ps

comb :: Int -> [a] -> [[a]]
comb 0 _      = [[]]
comb _ []     = []
comb m (x:xs) = map (x:) (comb (m-1) xs) ++ comb m xs

parseItems :: String -> [Int]
parseItems is = map readint (words is)
    where readint x = read x :: Int
