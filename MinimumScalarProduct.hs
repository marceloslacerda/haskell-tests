import Data.List
import Data.Ord (comparing)
import Data.Int(Int64)

type Case = ([Int64], [Int64])

main = interact (unlines
	. run
	. lines)

run :: [String] -> [String]
run (x:xs) =
    map (\(c, p) -> "Case #" ++ show c ++ ": " ++ show p)
    $ zip [1..]
    $ map (\(l1, l2) -> msp l1 l2)
    $ parseCases xs

parseCases :: [String] -> [Case]
parseCases [] = []
parseCases (_:l1:l2:ls) = (parseCase l1 l2) : parseCases ls

parseCase :: String -> String -> Case
parseCase l1 l2 = (map read $ words l1, map read $ words l2)

msp l1 l2 =
    let sorted1 = sortBy (flip compare) l1
        sorted2 = sort l2
    in sum $ map (\(a,b) -> a*b)
                 $ zip sorted1 sorted2
