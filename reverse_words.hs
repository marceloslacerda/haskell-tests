main = interact (unlines
	. run
	. lines)

run (x:xs) = 
	map (\(c,s) -> "Case #" ++ show c ++ ": " ++ s)
	$ zip [1 .. (read x)]
	$ map unwords
	$ map reverse
	$ map words xs

